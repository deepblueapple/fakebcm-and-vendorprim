// FakeBCM

vector blue = <0.000, 0.455, 0.851>;
vector lime = <0.004, 1.000, 0.439>;
vector white = <1, 1, 1>;
string username;
integer DEV_MODE = FALSE;

string objectName;
string objectKey;
string avKey;
string avUsername;
string avDisplayname;
string objectURL;
integer coinBalance;
string lampServer = "https://[serverURL].com/fakebcm1.0/";
key http_in;
key http_out_checkAuth;
key http_out_moneyReset;
key http_out_purchaseConfirm;
integer purchaseListenChannel;
integer meterListenHandle;

integer purchaseListenHandle;
integer purchaseListenSecs = 0;
string purchaseItemName;
string purchaseItemAmount;
string purchaseItemOwnerUUID;
string purchaseItemUniqueCode;
integer purchaseConfirmationRequest = FALSE;


string strReplace(string source, string pattern, string replace) {
    while (llSubStringIndex(source, pattern) > -1) {
        integer len = llStringLength(pattern);
        integer pos = llSubStringIndex(source, pattern);
        if (llStringLength(source) == len) { source = replace; }
        else if (pos == 0) { source = replace+llGetSubString(source, pos+len, -1); }
        else if (pos == llStringLength(source)-len) { source = llGetSubString(source, 0, pos-1)+replace; }
        else { source = llGetSubString(source, 0, pos-1)+replace+llGetSubString(source, pos+len, -1); }
    }
    return source;
}


hideAlpha() {
            llSetLinkAlpha(2, 0.0, ALL_SIDES);
            llSetLinkAlpha(3, 0.0, ALL_SIDES);
}

showAlpha() {
            llSetLinkAlpha(2, 1.0, ALL_SIDES);
            llSetLinkAlpha(3, 1.0, ALL_SIDES);
}



mainInit() {
    meterListenHandle = 0;
    purchaseListenChannel;
    purchaseListenHandle = 0;
    purchaseListenSecs = 0;
    llSetObjectName("FakeBCM v1.0");
    objectName = "FakeBCM v1.0";
    objectKey = llGetKey();
    avKey = llGetOwner();
    avUsername = llGetUsername(llGetOwner());
    avDisplayname = llGetDisplayName(llGetOwner());
    http_in = llRequestURL();
}

default
{
    state_entry()
    {
        hideAlpha();
        llSetText(" * FakeBCM v1.0 * \n", white, 1);
        
       if(llGetOwner() == "6590c28d-3372-4d51-a096-04919fd3f7ea") {
            llSleep(5);
        }
        
        
        if(llGetAttached() != 9 && !DEV_MODE) {
          state nonHudUse;
        } else {
          state authentication;
        } 

    }

}



    state authentication {

        state_entry() {
                   llSetText("FakeBCM initializing...\n", white, 1);
                    mainInit();
        }


        http_request(key id, string method, string body) {

            if(id != http_in) {
                return;
            }

            if(method == URL_REQUEST_GRANTED) {
                objectURL = body;
                string params = "objectName="+ objectName+"&objectKey="+objectKey+"&avKey="+avKey+"&avUsername="+avUsername+"&avDisplayname="+avDisplayname+"&objectURL="+objectURL;
                http_out_checkAuth = llHTTPRequest( lampServer + "auth.php", [HTTP_METHOD, "POST", HTTP_MIMETYPE,"application/x-www-form-urlencoded"], params);
            } else if(method == URL_REQUEST_DENIED) {
                llOwnerSay("Region did not provide a URL for this object. Please detatch and try again later.");
            } else {
                llOwnerSay("Region did not provide a URL for this object. Please detatch and try again later. Region says: " + body);
            }
        }


    http_response(key id, integer status, list meta, string body) {

      //  llOwnerSay(body);

        if ( id != http_out_checkAuth ) {
            return;
        }

        if (id == NULL_KEY) {
            llOwnerSay("Too many HTTP requests - please detatch and try again later.");
            state registrationError;
        } else if ( status == 499 ) {
            llOwnerSay("Request timed out. Detech and try again later.");
            state registrationError;
        } else if ( status == 404 ) {
            llOwnerSay("The server is down - please detatch and try later or notify DeepBlueApple Resident");
            state registrationError;
        } else if ( status != 200 ) {
            llOwnerSay("Request invalid. Detatch and try again later.");
            state registrationError;
        } else if (body == "0") {
            llOwnerSay("The server threw an error. Please de-tatch and try again later, or if it keeps happening contact DeepBlueApple Resident.");  
            state registrationError;
        } else if (body == "3") {
            llOwnerSay("Demo expired or your FakeBCM version is no longer supported.");  
            state registrationError;
        } else if (body == "4") {
            llOwnerSay("Problem with the SQL backend - please try again later.");  
            state registrationError;
        } else if (llGetSubString(body, 0, 16) == "@@@FAKEBCMRESP@@@") {
            
            string bodyData = llGetSubString(body, 17, -1); 
            list replyList = llParseString2List(bodyData,["|"],[]);
            integer x = 0;
            integer newReg = FALSE;;
            for(x = 0; x < llGetListLength(replyList); x = x + 2) {

                string itemKey = llList2String(replyList, x);

                if(itemKey == "NewRegistration") {
                    newReg = TRUE;;
                    llOwnerSay("New account registered for " + avUsername + "! You can reset your coin balance to BC$200 at any time by typing '/1 fbcm money-reset'");
                }

                if(itemKey == "Upgrade") {
                    llOwnerSay("A new version of FakeBCM is available.");
                }

                if(itemKey == "VersionExpire") {
                    llOwnerSay("This version of FakeBCM will expire on " + llList2String(replyList, x+1));
                }

                if(itemKey == "DemoExpire") {
                    llOwnerSay("This demo will expire and you will not be able to use this item from " + llList2String(replyList, x+1));
                }

                if(itemKey == "Coin") {
                    coinBalance = llList2Integer(replyList, x+1);
                }
           }                
            
            if (!newReg) {
                    llOwnerSay("Welcome back " + avUsername + "! Remember that you can reset your coin balance to BC$200 at any time by typing '/1 fbcm money-reset'");

            }

            state normal;            

        } else {
            state registrationError;
            return;
        }

    }   


        on_rez(integer n) {
            state default;
        }

    }


    state nonHudUse {

        state_entry() {
            llSetText("De-rezz me, and attach me to your spine.", white, 1); 
            llOwnerSay("I only work when attached to your spine.");
            showAlpha();
        }

        on_rez(integer n) {
            state default;
        }
    }


    state registrationError {

        state_entry() {
                        llSetText("Registration Error", white, 1); 
        }

        on_rez(integer n) {
            state default;
        }
    }


    state normal {
        state_entry() {
            meterListenHandle = llListen(1, "", llGetOwner(), "");
            llSetText("FakeBCM\n" + avDisplayname + "\n$" + (string)coinBalance, lime, 1);
        }

        touch_start( integer num_detected )
        {
            // if there was any touching... block it whilst a purchase request is in progress
            if(purchaseConfirmationRequest) {
                return;
            }

            // rest of touch code below remains...
        }

        listen( integer channel, string name, key id, string message )
        {

            if(channel == purchaseListenChannel) {
                llSetTimerEvent(0);
                llListenRemove(purchaseListenHandle);
                if(message == "Confirm") {
                    string params = "avKey="+avKey+"&action=confirm&price="+purchaseItemAmount+"&purchaseItemOwnerUUID="+purchaseItemOwnerUUID+"&purchaseItemUniqueCode="+purchaseItemUniqueCode;
                    http_out_purchaseConfirm = llHTTPRequest( lampServer + "fbcm2vp.php", [HTTP_METHOD, "POST", HTTP_MIMETYPE,"application/x-www-form-urlencoded"], params);                
                } else if (message == "Cancel") {
                    string params = "avKey="+avKey+"&action=cancel&purchaseItemOwnerUUID="+purchaseItemOwnerUUID+"&purchaseItemUniqueCode="+purchaseItemUniqueCode;
                    http_out_purchaseConfirm = llHTTPRequest( lampServer + "fbcm2vp.php", [HTTP_METHOD, "POST", HTTP_MIMETYPE,"application/x-www-form-urlencoded"], params);                
                }
                purchaseListenSecs = 0;                
                llSetTimerEvent(0);
                purchaseItemName = "";
                purchaseItemAmount = "";
                purchaseItemOwnerUUID = "";
                purchaseItemUniqueCode = "";
                purchaseConfirmationRequest = FALSE;
                return;
            }

            // only other handle will be the one listening on /1

            if(llGetSubString(message, 0, 4) != "fbcm ") {
                return;
            }

            string command = llGetSubString(message, 5, -1);

            if(command == "reset") {
                state default;
            }

            if(command == "money-reset") {
                string params = "objectName="+ objectName+"&objectKey="+objectKey+"&avKey="+avKey+"&avUsername="+avUsername+"&avDisplayname="+avDisplayname+"&objectURL="+objectURL;
                http_out_moneyReset = llHTTPRequest( lampServer + "moneyReset.php", [HTTP_METHOD, "POST", HTTP_MIMETYPE,"application/x-www-form-urlencoded"], params);                
            }

        }


        http_request(key id, string method, string body) {

            string convertedBody = llUnescapeURL(body);
            if(method == "POST") {

                if(llGetSubString(convertedBody, 0, 21) == "data=@@@FAKEBCMRESP@@@") {
                    string bodyData = llGetSubString(convertedBody, 22, -1);
                    list replyList = llParseString2List(bodyData,["|"],[]);
                    integer x = 0;

                    for(x = 0; x < llGetListLength(replyList); x = x + 2) {

                        string itemKey = llList2String(replyList, x);

                        if(itemKey == "Coin") {
                            coinBalance = llList2Integer(replyList, x+1);
                        }

                        if(itemKey == "Say") {
                            string sayText = llUnescapeURL(llList2String(replyList, x+1));
                            llOwnerSay(strReplace(sayText, "+", " "));
                        }

                        if(itemKey == "PurchaseConfirmationItemDesc") {
                            purchaseItemName = llList2String(replyList, x+1);
                        }

                        if(itemKey == "PurchaseConfirmationItemAmount") {
                            purchaseItemAmount = llList2String(replyList, x+1);
                            purchaseConfirmationRequest = TRUE;
                        }

                        if(itemKey == "PurchaseConfirmationItemOwnerUUID") {
                            purchaseItemOwnerUUID = llList2String(replyList, x+1);
                        }

                        if(itemKey == "PurchaseConfirmationItemUniqueCode") {
                            purchaseItemUniqueCode = llList2String(replyList, x+1);
                        }

                    }


                    llSetText("FakeBCM\n" + avDisplayname + "\n$" + (string)coinBalance, lime, 1);
            
                    if(purchaseConfirmationRequest) {
                        purchaseListenChannel = (integer)llFrand(-10000)-100;
                        llDialog(llGetOwner(), "You are about to purchase '" + strReplace(purchaseItemName, "+", " ") + "' for $" + (string)purchaseItemAmount, ["Confirm", "Cancel" ] , purchaseListenChannel);
                        purchaseListenHandle = llListen(purchaseListenChannel, "", llGetOwner(), "");
                        purchaseListenSecs = 13;
                        llSetTimerEvent(1);
                    }

                }

                llHTTPResponse(id, 200, "OK");
            }
            
        }

    http_response(key id, integer status, list meta, string body) {
        
        if ( status == 499 ) {
            llOwnerSay("Request timed out. Detech and try again later.");
            state registrationError;
        } else if ( status == 404 ) {
            llOwnerSay("The server is down - please detatch and try later or notify DeepBlueApple Resident");
            state registrationError;
        } else if ( status != 200 ) {
            llOwnerSay("Request invalid. Detatch and try again later.");
            state registrationError;
        } else if (body == "0") {
            llOwnerSay("The server threw an error. Please de-tatch and try again later, or if it keeps happening contact DeepBlueApple Resident.");  
            state registrationError;
        } else if (body == "4") {
            llOwnerSay("Problem with the SQL backend - please try again later.");  
            state registrationError;
        } else if (llGetSubString(body, 0, 16) == "@@@FAKEBCMRESP@@@") {
            
            string bodyData = llGetSubString(body, 17, -1); 
            list replyList = llParseString2List(bodyData,["|"],[]);
            integer x = 0;
            for(x = 0; x < llGetListLength(replyList); x = x + 2) {

                string itemKey = llList2String(replyList, x);

                if(itemKey == "Coin") {
                    coinBalance = llList2Integer(replyList, x+1);
                    llSetText("FakeBCM\n" + avDisplayname + "\n$" + (string)coinBalance, lime, 1);
                }

                if(itemKey == "CoinReset") {
                    coinBalance = llList2Integer(replyList, x+1);
                    llOwnerSay("Your coin balance has been reset to $" + (string)coinBalance);
                    llSetText("FakeBCM\n" + avDisplayname + "\n$" + (string)coinBalance, lime, 1);
                }


                if(itemKey == "PurchaseEvent" && llList2String(replyList, x+1) == "Fail") {
                    llOwnerSay("You do not have enough funds to complete this transaction.");
                }                

           }        

        }

    }           


    timer()
    {
        if(purchaseListenSecs > 0) {
            --purchaseListenSecs;
        } else {
            llListenRemove(purchaseListenHandle);
            purchaseListenSecs = 0;
            llSetTimerEvent(0);
            llOwnerSay("Purchase opportunity expired, please try again.");
            purchaseItemName = "";
            purchaseItemAmount = "";
            purchaseItemOwnerUUID = "";
            purchaseItemUniqueCode = "";
            purchaseConfirmationRequest = FALSE;
        }
    }

    on_rez(integer n) {
        state default;
    }



    }
