// VendorPrim

string configurationNotecardName = "CONFIG";
key notecardQueryId;
integer line;

vector white = <1, 1, 1>;

string mode = "DEFAULT";
string itemType = "DEFAULT";
string itemName = "DEFAULT";
integer price = 0;
string successMsg = "DEFAULT";
string failMsg = "DEFAULT";
string uniqueCode = "DEFAULT";
string customerUsername = "DEFAULT";
key customerUUID = NULL_KEY;
integer missingObjectErr = FALSE; 
integer ownerMenuListenChannel;
integer ownerMenuListener;
integer ownerMenuActive = FALSE;
integer ownerMenuListenTimer = 0;
integer purchaseTimer = 0;
integer purchaseSuccess = FALSE;

string lampServer = "https://lamp.thelogcleaner.com/vendorprim1.0/";

string objectURL;
key http_in;
key http_out_auth;
key http_out_itemReport;
key http_out_purchaseRequest;


sendPurchaseRequest(key customer) {
        customerUsername = llGetUsername(customer);
        llSetText("Sending purchase request to \n" + customerUsername + "...", white, 1);
        purchaseTimer = 15;
        string params = "unique=" + uniqueCode + "&objectOwnerUUID=" + (string)llGetOwner() + "&itemName=" + itemName + "&price=" + (string)price + "&customerUUID=" + (string)customer;
        http_out_purchaseRequest = llHTTPRequest(lampServer + "vp2fbcm.php", [HTTP_METHOD, "POST", HTTP_MIMETYPE,"application/x-www-form-urlencoded"], params);
        llSetTimerEvent(1);
}



init()
{
 
mode = "DEFAULT";
itemType = "DEFAULT";
itemName = "DEFAULT";
price = 0;
successMsg = "DEFAULT";
failMsg = "DEFAULT";
uniqueCode = "DEFAULT";
missingObjectErr = FALSE;
customerUsername = "";
customerUUID = NULL_KEY;
purchaseTimer = 0;
purchaseSuccess = FALSE;

//  make sure the file exists and is a notecard
    if(llGetInventoryType(configurationNotecardName) != INVENTORY_NOTECARD)
    {
    //  notify owner of missing file
        llOwnerSay("Missing inventory notecard: " + configurationNotecardName);
    //  don't do anything else
        state invalidConfig;
        return;
    }
 
//  initialize to start reading from first line (which is 0)
    line = 0;
    notecardQueryId = llGetNotecardLine(configurationNotecardName, line);
}


firstError() {
    llOwnerSay("The CONFIG notecard contains the errors below:-");
}

processConfiguration(string data)
{
//  if we are at the end of the file
    if(data == EOF)
    {
    //  do not do anything else

    integer ncErrors = FALSE;
    integer firstErrorDisplayed = FALSE;

    if(mode == "DEFAULT") {
        ncErrors = TRUE;
        if(!firstErrorDisplayed) {
            firstErrorDisplayed = TRUE;
            firstError();          
        }
        llOwnerSay(" * Mode parameter invalid");
    }

    if(itemName == "DEFAULT" && mode != "Listen") {
        ncErrors = TRUE;
        if(!firstErrorDisplayed) {
            firstErrorDisplayed = TRUE;
            firstError();          
        }
        llOwnerSay(" * ItemName parameter invalid");
    }


    if(price == 0 && mode != "Listen") {
        ncErrors = TRUE;
        if(!firstErrorDisplayed) {
            firstErrorDisplayed = TRUE;
            firstError();          
        }
        llOwnerSay(" * Price parameter invalid");
    }

    if(successMsg == "DEFAULT" && mode != "Listen") {
        ncErrors = TRUE;
        if(!firstErrorDisplayed) {
            firstErrorDisplayed = TRUE;
            firstError();          
        }
        llOwnerSay(" * Success parameter invalid");
    }

    if(failMsg == "DEFAULT" && mode != "Listen") {
        ncErrors = TRUE;
        if(!firstErrorDisplayed) {
            firstErrorDisplayed = TRUE;
            firstError();          
        }
        llOwnerSay(" * Fail parameter invalid");
    }

    if(uniqueCode == "DEFAULT") {
        ncErrors = TRUE;
        if(!firstErrorDisplayed) {
            firstErrorDisplayed = TRUE;
            firstError();          
        }
        llOwnerSay(" * Unique parameter invalid");
    }


    if(ncErrors) {
        state invalidConfig;
    }


    if(mode == "Listen") {
        state listenMode;
    } else {
        if(itemType == "Physical" && llGetInventoryNumber(INVENTORY_OBJECT) != 1) {
            missingObjectErr = TRUE;
            llOwnerSay("Error: As the Item Type is Physical, the BC VendorPrim must contain an object (and, only one object)");
            state invalidConfig;
        } else {
            state touchModeRegister;
        }
    }

    return;
 
    }
 
//  if we are not working with a blank line
    if(data != "")
    {
    //  if the line does not begin with a comment
        if(llSubStringIndex(data, "#") != 0)
        {
        //  find first equal sign
            integer i = llSubStringIndex(data, "@@@");
 
        //  if line contains equal sign
            if(i != -1)
            {

            //  trim value

                list temp = llParseString2List(data, ["@@@"], []);
                string value1 = llList2String(temp, 0);
                string value2 = llList2String(temp, 1);

                if(value1 == "Mode") {
                    if(value2 == "Touch") {
                        mode = value2;
                    } else if (value2 = "Listen") {
                        mode = value2;
                        state listenMode;
                    } else {
                        state invalidConfig;
                    }
                }


                if(value1 == "ItemType") {
                    if(value2 == "Physical") {
                        itemType = value2;
                    } else if (value2 = "Service") {
                        mode = value2;
                    } else {
                        state invalidConfig;
                    }
                }


                if(value1 == "ItemName") {
                    string tempItemName = llStringTrim(value2, STRING_TRIM);
                    if(llStringLength(tempItemName) >= 5 && llStringLength(tempItemName) <= 60) {
                        itemName = tempItemName;
                    } else {
                        state invalidConfig;
                    }
                }


                if(value1 == "Price") {          
                   if((integer)value2 && ((integer)value2 >= 10 && (integer)value2  <= 1000)) {
                       price = (integer)value2;
                    } else {
                        state invalidConfig;
                    }
                }

                if(value1 == "Success") {
                    string tempSuccessMsg = llStringTrim(value2, STRING_TRIM);
                    if(llStringLength(tempSuccessMsg) >= 5 && llStringLength(tempSuccessMsg) <= 180) {
                        successMsg = tempSuccessMsg;
                    } else {
                        state invalidConfig;
                    }
                }

                if(value1 == "Fail") {
                    string tempFailMsg = llStringTrim(value2, STRING_TRIM);
                    if(llStringLength(tempFailMsg) >= 5 && llStringLength(tempFailMsg) <= 180) {
                        failMsg = tempFailMsg;
                    } else {
                        state invalidConfig;
                    }
                }

                if(value1 == "Unique") {
                    string tempUniqueCode = llStringTrim(value2, STRING_TRIM);
                    if(llStringLength(tempUniqueCode) >= 8 && llStringLength(tempUniqueCode) <= 8) {
                        uniqueCode = tempUniqueCode;
                    } else {
                        state invalidConfig;
                    }
                }

            }
        //  line does not contain equal sign
            else
            {
                llOwnerSay("Configuration could not be read on line " + (string)line);
            }
        }
    }
 
//  read the next line
    notecardQueryId = llGetNotecardLine(configurationNotecardName, ++line);
}
 
default
{
    on_rez(integer start_param)
    {
        init();
    }
 
    changed(integer change)
    {
        if(change & (CHANGED_OWNER | CHANGED_INVENTORY))
            init();
    
        if (change & CHANGED_REGION_START)
        {
            state default;
        }
    
    }

 
    state_entry()
    {
        init();
    }
 
    dataserver(key request_id, string data)
    {
        if(request_id == notecardQueryId)
            processConfiguration(data);
    }

    
}


state invalidConfig {
    state_entry() {
        if(!missingObjectErr) {
            llOwnerSay("Your CONFIG notecard is invalid or not completed - go to https://bcvendorprimconfigurator.web.app to generate a string to paste in the CONFIG notecard.");   
        } 
    }

    touch_start(integer total_number)
    {
        llSay(0, "This item is not selling at the moment; invalid config.");
    }

    changed(integer change)
    {
        if(change & (CHANGED_OWNER | CHANGED_INVENTORY))
            state default;

        if (change & CHANGED_REGION_START)
        {
            state default;
        }

    }


    on_rez(integer start_param)
    {
        state default;
    }    

}

state listenMode {
    state_entry() {
        llOwnerSay("Listen mode not operational in this demo product :(");
        llSetText("Nope :(", white, 1);
    }

    changed(integer change)
    {
        if(change & (CHANGED_OWNER | CHANGED_INVENTORY))
            state default;
    }

}


state touchModeRegister {

    state_entry() {
        llSetText("Registering...", white, 1);
        http_in = llRequestURL();
    }

    http_request(key id, string method, string body) {

        if(id != http_in) {
            return;
        }

        if(method == URL_REQUEST_GRANTED) {
            string params = "unique=" + uniqueCode + "&objectName=" + llGetObjectName() + "&objectKey=" + (string)llGetKey() + "&objectOwnerUUID=" + (string)llGetOwner() + "&objectOwnerUsername=" + llGetUsername(llGetOwner()) + "&objectOwnerDisplayname=" + llGetDisplayName(llGetOwner()) + "&objectURL=" + body + "&objectLastLocation=" + llGetRegionName();
            http_out_auth = llHTTPRequest(lampServer + "auth.php", [HTTP_METHOD, "POST", HTTP_MIMETYPE,"application/x-www-form-urlencoded"], params);
        }
        
    }

    
    http_response(key id, integer status, list meta, string body) {

        if ( id != http_out_auth ) {
            return;
        }

        if (id == NULL_KEY) {
            llOwnerSay("Too many HTTP requests - please detatch and try again later.");
            state registrationFailed;
        } else if ( status == 499 ) {
            llOwnerSay("Request timed out. Detech and try again later.");
            state registrationFailed;
        } else if ( status == 404 ) {
            llOwnerSay("The server is down - please detatch and try later or notify DeepBlueApple Resident");
            state registrationFailed;
        } else if ( status != 200 ) {
            llOwnerSay("Request invalid. Detatch and try again later.");
            state registrationFailed;
        } else if (body == "0") {
            llOwnerSay("The server threw an error. Please de-tatch and try again later, or if it keeps happening contact DeepBlueApple Resident.");  
            state registrationFailed;
        } else if (body == "3") {
            llOwnerSay("Demo expired or your version is no longer supported.");  
            state registrationFailed;
        } else if (body == "4") {
            llOwnerSay("Problem with the SQL backend - please try again later.");  
            state registrationFailed;
        } else if (body == "5") {
            llOwnerSay("Not registered: You cannot sell with VendorPrim in this region.");
            state registrationFailed;
        } else if (llGetSubString(body, 0, 7) == "@@@VP@@@") {

            integer registered = FALSE;
            integer newReg = FALSE;    
            string bodyData = llGetSubString(body, 8, -1); 
            list replyList = llParseString2List(bodyData,["|"],[]);
            integer x = 0;

            for(x = 0; x < llGetListLength(replyList); x = x + 2) {

                string itemKey = llList2String(replyList, x);

                if(itemKey == "Registered") {
                    registered = TRUE;
                    llOwnerSay("Registered successfully.");
                }

                if(itemKey == "NewRegistration") {
                    newReg = TRUE;
                    llOwnerSay("New VendorPrim object registered for " + llGetUsername(llGetOwner()) + "!");
                }

            } 

            if(registered) {
                state touchMode;
            }

        } else {
            llOwnerSay("Problem registering - please re-rezz and try again later.");  
            state registrationFailed;
        }



    }

    changed(integer change)
    {
        if(change & (CHANGED_OWNER | CHANGED_INVENTORY))
            state default;
    }

   on_rez(integer start_param)
    {
        state default;
    }  
    
}

state registrationFailed {
    state_entry() {
        llSetText("Registration failed", white, 1);
    }


    changed(integer change)
    {
        if(change & (CHANGED_OWNER | CHANGED_INVENTORY))
            state default;

        if (change & CHANGED_REGION_START)
        {
            state default;
        }
            
    }

   on_rez(integer start_param)
    {
        state default;
    }  


}

state touchMode {
       state_entry() {
         llSetText("", white, 1);
       } 

    touch_start( integer num_detected )
    {
     if(purchaseTimer == 0 && ownerMenuActive == FALSE) {
       if(llDetectedKey(0) == llGetOwner()) {
                llSetText("Listening to owner...", white, 1);
                ownerMenuListenChannel = (integer)llFrand(-10000)-100;
                ownerMenuActive = TRUE;
                ownerMenuListenTimer = 15;
                llSetTimerEvent(1);
                ownerMenuListener = llListen(ownerMenuListenChannel, "", llGetOwner(), "");
                llDialog(llGetOwner(), "OWNER MENU\n\nWould you like to see an item report or purchase the item?", ["Item Report", "Purchase", "Cancel"], ownerMenuListenChannel); 
         } else {
                sendPurchaseRequest(llDetectedKey(0));
         }
     }

    }

    listen( integer channel, string name, key id, string message )
    {
        if(channel == ownerMenuListenChannel) {
            llSetTimerEvent(0);
            ownerMenuActive = FALSE;
            llListenRemove(ownerMenuListener);
            ownerMenuListenChannel = 0;
            ownerMenuListenTimer = 0;
            llSetText("", white, 1);
            if(message == "Item Report") {
                llOwnerSay("Your item report is being generated, this will take a few moments...");
                string params = "unique=" + uniqueCode + "&objectOwnerUUID=" + (string)llGetOwner();
                http_out_itemReport = llHTTPRequest(lampServer + "itemReport.php", [HTTP_METHOD, "POST", HTTP_MIMETYPE,"application/x-www-form-urlencoded"], params);
            } else if(message == "Purchase") {
                sendPurchaseRequest(id);
            }
        }
    }

        http_response( key request_id, integer status, list metadata, string body )
        {
            if(request_id == http_out_purchaseRequest) {
                if(body == "@@@VP@@@|Debug|OK") {
                    llSetText("Purchase request sent to\n" + customerUsername + " - awaiting acceptance", white, 1);
                } else {
                    llSetText("Cannot find an online fBCM belonging to\n" + customerUsername + " - purchase cancelled", white, 1);
                    purchaseTimer = 2;
                }
            customerUsername = "";
            }

            if(request_id == http_out_itemReport) {
                    if ( status == 499 ) {
                        llOwnerSay("Request timed out. Detech and try again later.");
                        state registrationFailed;
                    } else if ( status == 404 ) {
                        llOwnerSay("The server is down - please detatch and try later or notify DeepBlueApple Resident");
                        state registrationFailed;
                    } else if ( status != 200 ) {
                        llOwnerSay("Request invalid. Detatch and try again later.");
                        state registrationFailed;
                    } else if (body == "0") {
                        llOwnerSay("The server threw an error. Please de-tatch and try again later, or if it keeps happening contact DeepBlueApple Resident.");  
                        state registrationFailed;
                    } else if (body == "3") {
                        llOwnerSay("Demo expired or your version is no longer supported.");  
                        state registrationFailed;
                    } else if (body == "4") {
                        llOwnerSay("Problem with the SQL backend - please try again later.");  
                        state registrationFailed;
                    } else if (body == "5") {
                        llOwnerSay("Not registered: You cannot sell with VendorPrim in this region.");
                        state registrationFailed;
                    } else if (llGetSubString(body, 0, 7) == "@@@VP@@@") {

                        string reportLine1;
                        string reportLine2;
                        string reportLine3;
                        string reportLine4;
                        string reportLine5;

                        string bodyData = llGetSubString(body, 8, -1); 
                        list replyList = llParseString2List(bodyData,["|"],[]);
                        integer x = 0;

                        for(x = 0; x < llGetListLength(replyList); x = x + 2) {

                            string itemKey = llList2String(replyList, x);

                            if(itemKey == "ReportLine1") {
                                reportLine1 = llList2String(replyList, x+1); 
                            }

                            if(itemKey == "ReportLine2") {
                                reportLine2 = llList2String(replyList, x+1); 
                            }

                            if(itemKey == "ReportLine3") {
                                reportLine3 = llList2String(replyList, x+1); 
                            }

                            if(itemKey == "ReportLine4") {
                                reportLine4 = llList2String(replyList, x+1); 
                            }

                            if(itemKey == "ReportLine5") {
                                reportLine5 = llList2String(replyList, x+1); 
                            }

                        }

                        llOwnerSay(reportLine1 + "\n" + reportLine2 + "\n" + reportLine3 + "\n" + reportLine4 + "\n" + reportLine5);
    
                    }
            
                }
        }

        http_request( key request_id, string method, string body )
        {

            if(method != "POST") {
                return;
            }

           string convertedBody = llUnescapeURL(body);
           if(llGetSubString(convertedBody, 0, 12) == "data=@@@VP@@@") {            
                llHTTPResponse(request_id, 200, "OK");
                string bodyData = llGetSubString(convertedBody, 13, -1); 
                list replyList = llParseString2List(bodyData,["|"],[]);
                integer x = 0;
                for(x = 0; x < llGetListLength(replyList); x = x + 2) {
                    string itemKey = llList2String(replyList, x);    
                    if(itemKey == "PurchaseEvent") {
                            string itemValue = llList2String(replyList, x+1);
                            if(itemValue == "Cancel") {
                                llSetTimerEvent(0);
                                purchaseTimer = 0;
                                llSetText("", white, 1);
                            } else if(itemValue == "Fail") {
                                llSay(0, failMsg);
                                llSetTimerEvent(0);
                                purchaseTimer = 0;
                                llSetText("", white, 1);
                            } else if(itemValue == "Success") {
                                purchaseSuccess = TRUE;
                                if(itemType == "Physical") {
                                    llSay(0, successMsg);
                                    llSetText("Sending item...", white, 1);
                                    purchaseTimer = 2;
                                    llSetTimerEvent(1);
                                } else {
                                    llSay(0, successMsg);
                                    llSetTimerEvent(0);
                                    purchaseTimer = 0;
                                    llSetText("", white, 1);
                                }
                            }
                    }

                    if(itemKey == "CustomerUUID") {
                        customerUUID = llList2Key(replyList, x+1);
                    }

                }

                if(purchaseSuccess) {
                    if(itemType == "Physical") {
                        llGiveInventory(customerUUID, llGetInventoryName(INVENTORY_OBJECT, 0));
                    }
                    purchaseSuccess = FALSE;
                    customerUUID = NULL_KEY;
                }


           }
    
    }


    timer() {

        if(ownerMenuActive && ownerMenuListenTimer > 0) {
            --ownerMenuListenTimer;
            return;
        } else if(ownerMenuActive && ownerMenuListenTimer <= 1) {
            llSetTimerEvent(0);
            llListenRemove(ownerMenuListener);
            ownerMenuActive = FALSE;
            ownerMenuListenChannel = 0;
            ownerMenuListenTimer = 0;
            llSetText("", white, 1);
            llOwnerSay("Dialog has timed out");
            return;
        }

        if(purchaseTimer > 0) {
            --purchaseTimer;
        } else {
            llSetTimerEvent(0);
            purchaseTimer = 0;
            llSetText("", white, 1);
        }
    }

    changed(integer change)
    {
        if(change & (CHANGED_OWNER | CHANGED_INVENTORY))
            state default;

        if (change & CHANGED_REGION_START)
        {
            state default;
        }
            
    }

   on_rez(integer start_param)
    {
        state default;
    }  
}

