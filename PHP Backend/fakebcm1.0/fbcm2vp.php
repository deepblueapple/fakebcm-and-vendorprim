<?php

// fakebcm1.0/fbcm2vp.php


// if these vars are not on the get, do sod all

if( !( isset($_POST['avKey']) && isset($_POST['action']) && isset($_POST['purchaseItemOwnerUUID']) && isset($_POST['purchaseItemUniqueCode']))) {
	die("0a");
}

if($_POST['action'] == "confirm") {
	if(!(isset($_POST['price']))) {
		die("0b");
	}
}


// get version
$customerUUID = $_POST['avKey'];
$action = $_POST['action'];
$vpOwnerUUID = $_POST['purchaseItemOwnerUUID'];
$vpUniqueCode = $_POST['purchaseItemUniqueCode'];

if($action == "confirm") {
	$price = $_POST['price'];
}


// database creds
$servername = "localhost";
$username = "[username]";
$password = "[password]";
$dbname = "[database]";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);


// Check connection
if ($conn->connect_error) {
  die("4");
} 



// if action is cancel, send a message to the VendorPrim so it times out right away

$vp_sql = "SELECT *, (SELECT username FROM authuser WHERE userUUID = vendorprim.customTaxReceiverUUID) AS customTaxReceiverUsername from vendorprim WHERE objectOwnerUUID = \"" . $vpOwnerUUID . "\" AND uniqueCode = \"" . $vpUniqueCode . "\" AND disabled IS NULL LIMIT 1";

$result = $conn->query($vp_sql);

if ($result->num_rows > 0) {
	$vpRecord = mysqli_fetch_assoc($result);
	$vpOwnerUsername = $vpRecord['objectOwnerUsername'];
	$vpURL = $vpRecord['objectURL'];
	$customTaxRate = $vpRecord['customTaxRate'];
	$customTaxReceiverUUID = $vpRecord['customTaxReceiverUUID'];
	$customTaxReceiverUsername = $vpRecord['customTaxReceiverUsername'];
	$vpRegion = $vpRecord['objectLastLocation'];
} else {
   die("0bbb");
}
if($action == "cancel") {
     $data = array('data' => '@@@VP@@@PurchaseEvent|Cancel');
     $options = array(
             'http' => array(
             'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
             'method'  => 'POST',
             'content' => http_build_query($data)
         )
     );
 
     $context  = stream_context_create($options);
     $result = file_get_contents($vpURL, false, $context);
     die("@@@FAKEBCMRESP@@@");
}


// not died... must be a 'confirm'

// check if customer has enough funds, die here if not

$customer_sql = "SELECT * FROM authuser WHERE userUUID = \"" . $customerUUID . "\" AND disabled IS NULL LIMIT 1";

$result = $conn->query($customer_sql);


if ($result->num_rows > 0) {
	$customerRecord = mysqli_fetch_assoc($result);
	$customer_URL = $customerRecord['objectURL'];
	$customer_coin = $customerRecord['coinBalance'];
	$customer_username = $customerRecord['username'];
} else {
   die("0aaa"); 
}

if($price > $customer_coin) { 

		$data = array('data' => '@@@VP@@@PurchaseEvent|Fail');
		$options = array(
        		'http' => array(
        		'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		        'method'  => 'POST',
		        'content' => http_build_query($data)
		    )
		);

		$context  = stream_context_create($options);
		$result = file_get_contents($vpURL, false, $context);
		die("@@@FAKEBCMRESP@@@Coin|$customer_coin|PurchaseEvent|Fail");
}


// well, funds aren't an issue... we already know the item price - lets find out the applicate tax rate

$taxPayable = false;

if($customTaxRate > 0 && $customTaxReceiverUUID != "") {
	$taxPayable = true;
	$taxRate = $customTaxRate;	
	$taxReceiverUUID = $customTaxReceiverUUID;
	$taxReceiverUsername = $customTaxReceiverUsername; 
} else {
	$taxTable_sql = "SELECT vendorprim_tax.taxRate, vendorprim_tax.taxReceiverUUID, authuser.username AS taxReceiverUsername FROM vendorprim_tax, authuser WHERE vendorprim_tax.region = \"" . $vpRegion. "\" AND authuser.userUUID = vendorprim_tax.taxReceiverUUID LIMIT 1";
	$result = $conn->query($taxTable_sql);

	if ($result->num_rows > 0) {
		$taxRecord = mysqli_fetch_assoc($result);
		$taxRate = $taxRecord['taxRate'];
		$taxReceiverUUID = $taxRecord['taxReceiverUUID'];
		$taxReceiverUsername = $taxRecord['taxReceiverUsername'];
		$taxPayable = true;
	}

}

if($taxPayable) {
	$grossAmount = $price;
	$taxAmount = round(($price * ($taxRate / 100)));
	$netAmount = $price-$taxAmount;
	$sql = "INSERT INTO vendorprim_trans (objectOwnerUUID, objectOwnerUsername, uniqueCode, customerUUID, customerUsername, region, gross, tax, net, taxUUID, taxUsername, type) VALUES (\"" . $vpOwnerUUID  . "\", \"" . $vpOwnerUsername  . "\", \"" . $vpUniqueCode . "\", \"" . $customerUUID . "\", \"" . $customer_username . "\", \"" . $vpRegion . "\", \"" . $grossAmount . "\", \"" . $taxAmount . "\", \"" . $netAmount . "\", \"" . $taxReceiverUUID . "\", \"" . $taxReceiverUsername . "\", \"SALE\")"; 
} else {
	$grossAmount = $price;
	$taxAmount = 0;
	$netAmount = $price;
	$sql = "INSERT INTO vendorprim_trans (objectOwnerUUID, objectOwnerUsername, uniqueCode, customerUUID, customerUsername, region, gross, tax, net, type) VALUES (\"" . $vpOwnerUUID  . "\", \"" . $vpOwnerUsername  . "\", \"" . $vpUniqueCode . "\", \"" . $customerUUID . "\", \"" . $customer_username . "\", \"" . $vpRegion . "\", \"" . $grossAmount . "\", \"" . $taxAmount . "\", \"" . $netAmount . "\", \"SALE\")"; 
}

$result = $conn->query($sql);

if(!$result) {
	die("0");
}


// update balance of customer

$sql = "UPDATE authuser SET coinBalance = (coinBalance-" . $price . ") WHERE userUUID = \"" . $customerUUID . "\" ";

$result = $conn->query($sql);

if(!$result) {
	die("0");
}

// update balance of vendor

$sql = "UPDATE authuser SET coinBalance = (coinBalance+" . $netAmount . ") WHERE userUUID = \"" . $vpOwnerUUID . "\"";

$result = $conn->query($sql);

if(!$result) {
	die("0");
}

// update balance of tax collector

if($taxPayable) {

	$sql = "UPDATE authuser SET coinBalance = (coinBalance+" . $taxAmount . ") WHERE userUUID = \"" . $taxReceiverUUID . "\"";

	$result = $conn->query($sql);

	if(!$result) {
		die("0");
	}


}



// get new coin balances of the relevent parties

$sql = "SELECT coinBalance FROM authuser WHERE userUUID = \"" . $customerUUID . "\""; 

$result = $conn->query($sql);

if(!$result) {
	die("0");
}


$dbCoinCustomer = mysqli_fetch_assoc($result);
$coin_customer = $dbCoinCustomer['coinBalance'];


$sql = "SELECT coinBalance, objectURL FROM authuser WHERE userUUID = \"" . $vpOwnerUUID . "\""; 

$result = $conn->query($sql);

if(!$result) {
	die("0");
}


$dbCoinVendor = mysqli_fetch_assoc($result);
$coin_vendor = $dbCoinVendor['coinBalance'];
$url_vendor = $dbCoinVendor['objectURL'];

if($taxPayable) {

	$sql = "SELECT coinBalance, objectURL FROM authuser WHERE userUUID = \"" . $taxReceiverUUID . "\""; 

	$result = $conn->query($sql);

	if(!$result) {
		die("0");
	}

	$dbCoinTaxCollector = mysqli_fetch_assoc($result);
	$coin_taxcollector = $dbCoinTaxCollector['coinBalance'];
	$url_taxcollector = $dbCoinTaxCollector['objectURL'];
}

		     // send success message to VP (taxcollectorUUID and prices for llRegionSayTo comms)

		     $data1 = array('data' => '@@@VP@@@PurchaseEvent|Success|CustomerUUID|' . $customerUUID);

		     $options1 = array(
			     'http' => array(
			     'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			     'method'  => 'POST',
			     'content' => http_build_query($data1)
			 )
		     );


		     $context1 = stream_context_create($options1);
		     $result1 = file_get_contents($vpURL, false, $context1);
		     
		     
		     
		     
		     
		     // send updated coin balance to fBCM of vendor

		     if($taxPayable) {
			     $say2 = urlencode('$' . $netAmount . ' added to your balance! ' . $customer_username . ' spent $' . $grossAmount . ' on one of your items, and ' . $taxReceiverUsername . ' received a $' . $taxAmount . ' tax payment.'); 
		     } else {
			     $say2 = urlencode('$' . $netAmount . ' added to your balance! ' . $customer_username . ' spent $' . $grossAmount . ' on one of your items (no tax was applicable).'); 
		     }

		     $data2 = array('data' => '@@@FAKEBCMRESP@@@Coin|' . $coin_vendor . '|Say|' . $say2);
		     $options2 = array(
			     'http' => array(
			     'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			     'method'  => 'POST',
			     'content' => http_build_query($data2)
			 )
		     );
 
		     $context2  = stream_context_create($options2);
		     $result2 = file_get_contents($url_vendor, false, $context2);

		     
		     
		     
		     
		     // send updated coin balance to fBCM of tax collector

		     if($taxPayable) {
			     $say3 = urlencode('You just received a tax payment of $' . $taxAmount . '. ' . $customer_username . ' spent $' . $grossAmount . ' on an item sold by ' . $vpOwnerUsername . '.'); 
			     $data3 = array('data' => '@@@FAKEBCMRESP@@@Coin|' . $coin_taxcollector . '|Say|' . $say3);
			     $options3 = array(
				     'http' => array(
				     'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				     'method'  => 'POST',
				     'content' => http_build_query($data3)
				 )
			     );
	 
			     $context3  = stream_context_create($options3);
			     $result3 = file_get_contents($url_taxcollector, false, $context3);

		     }


     die("@@@FAKEBCMRESP@@@Coin|" . $coin_customer);

?>
