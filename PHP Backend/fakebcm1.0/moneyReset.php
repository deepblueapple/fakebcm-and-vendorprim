<?php

// script accessed by FakeBCM

// if these vars are not on the get, do sod all
if( !(isset($_POST['objectName']) && isset($_POST['objectKey']) && isset($_POST['avKey']) && isset($_POST['avUsername']) && isset($_POST['avDisplayname']) && isset($_POST['objectURL'])) ){
	die("0");
}

// get vars
$objectName = $_POST['objectName'];
$objectKey = $_POST['objectKey'];
$avKey = $_POST['avKey'];
$avUsername = $_POST['avUsername'];
$avDisplayname = $_POST['avDisplayname'];
$objectURL = $_POST['objectURL'];

if($objectName !==  "FakeBCM v1.0") {
	die("0");
}

// database creds
$servername = "localhost";
$username = "[username]";
$password = "[password]";
$dbname = "[database]";


// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);


// Check connection
if ($conn->connect_error) {
  die("4");
} 

$sql = "SELECT * from authuser WHERE userUUID = \"" . $avKey . "\"";

//$sql = "INSERT INTO authuser (userUUID, username, displayname) VALUES ('123-456-789', 'deepblueapple Resident', 'Wayne Peters')";

$authorised = false;

$result = $conn->query($sql);

if ($result->num_rows > 0) {
	$authorised = true;
	$dbRecord_authuser = mysqli_fetch_assoc($result);
	$authuser_id = $dbRecord_authuser['id'];
	$disabled = $dbRecord_authuser['disabled'];
} else {
    $authorised = false;
}


if($authorised) {

	if($disabled) {
		die(0);
	}

	$sql = "UPDATE authuser SET objectName = \"" . $objectName . "\", objectUUID = \"" . $objectKey . "\", objectURL = \"" . $objectURL . "\", username = \"" . $avUsername . "\", displayname = \"" . $avDisplayname . "\", coinBalance = 200, lastAuth = CURRENT_TIMESTAMP() WHERE userUUID = \"" . $avKey . "\"";


	$result = $conn->query($sql);

		if (!$result) {
		    die("0");
		}

	die("@@@FAKEBCMRESP@@@CoinReset|200");
	// insert need to die with something useful
	
} else {

	die(0);

}


?>
