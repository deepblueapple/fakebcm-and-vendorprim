<?php

// script accessed by FakeBCM 
// if these vars are not on the get, do sod all
if( !( isset($_POST['unique']) && isset($_POST['objectOwnerUUID']))){
   die("0");
}


// get version
$uniqueCode = $_POST['unique'];
$objectOwnerUUID = $_POST['objectOwnerUUID'];

// database creds
$servername = "localhost";
$username = "[username]";
$password = "[password]";
$dbname = "[database]";

// Location check
// die(5);

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);


// Check connection
if ($conn->connect_error) {
  die("4");
} 
$sql = "SELECT * from vendorprim WHERE uniqueCode = \"" . $uniqueCode . "\" AND objectOwnerUUID = \"" . $objectOwnerUUID . "\" AND disabled IS NULL";


$result = $conn->query($sql);

if ($result->num_rows > 0) {
	$vpRecord = mysqli_fetch_assoc($result);
	$customTaxRate = $vpRecord['customTaxRate'];
	$customTaxReceiverUUID = $vpRecord['customTaxReceiverUUID'];
	$customTaxReceiverUsername = $vpRecord['customTaxReceiverUsername'];
	$vpRegion = $vpRecord['objectLastLocation'];
} else {
   die(0); 
}



$taxPayable = false;
$regionSpecific = false;

if($customTaxRate > 0 && $customTaxReceiverUUID != "") {
	$taxPayable = true;
	$taxRate = $customTaxRate;	
	$taxReceiverUUID = $customTaxReceiverUUID;
	$taxReceiverUsername = $customTaxReceiverUsername; 
} else {
	$taxTable_sql = "SELECT vendorprim_tax.taxRate, vendorprim_tax.taxReceiverUUID, authuser.username AS taxReceiverUsername FROM vendorprim_tax, authuser WHERE vendorprim_tax.region = \"" . $vpRegion. "\" AND authuser.userUUID = vendorprim_tax.taxReceiverUUID LIMIT 1";
	$result = $conn->query($taxTable_sql);

	if ($result->num_rows > 0) {
		$regionSpecific = true;
		$taxRecord = mysqli_fetch_assoc($result);
		$taxRate = $taxRecord['taxRate'];
		$taxReceiverUUID = $taxRecord['taxReceiverUUID'];
		$taxReceiverUsername = $taxRecord['taxReceiverUsername'];
		$taxPayable = true;
	}

}


if($taxPayable) {
	$line1 = "Tax Rate: The tax rate deducted from revenue is " . $taxRate . "%. ";
	if($regionSpecific) {
		$line1 = $line1 . "This rate is region specific. Tax in this region is paid to " . $taxReceiverUsername;
	} else {
		$line1 = $line1 . "This rate is item specific. Tax for this item is paid to " . $taxReceiverUsername;
	}
}


$sql = "SELECT customerUsername, gross, tax, net, DATEDIFF(now(), timestamp) AS days FROM vendorprim_trans WHERE uniqueCode = \"" . $uniqueCode . "\" AND objectOwnerUUID = \"" . $objectOwnerUUID . "\" ORDER BY id DESC LIMIT 1";


	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		$lastCustomer = mysqli_fetch_assoc($result);
		$lastCustomerUsername = $lastCustomer['customerUsername'];
		$lastCustomerGross = $lastCustomer['gross'];
		$lastCustomerTax = $lastCustomer['tax'];
		$lastCustomerNet = $lastCustomer['net'];
		$lastCustomerDays = $lastCustomer['days'];
		$line2 = "Last purchase: The last purchase was by " . $lastCustomerUsername . " " . $lastCustomerDays . " day(s) ago; the price they paid was $" . $lastCustomerGross . " and you received a net of $" . $lastCustomerNet . " and paid $" . $lastCustomerTax . " in tax.";
	} else {
		$line2 = "Last purchase: No customers yet!";
	}


$sql = "SELECT SUM(gross) AS gross, SUM(tax) AS tax, SUM(net) AS net FROM vendorprim_trans WHERE (timestamp > DATE_SUB(now(), INTERVAL 1 DAY)) AND uniqueCode = \"" . $uniqueCode . "\" AND objectOwnerUUID = \"" . $objectOwnerUUID . "\"";


	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		$last24hours = mysqli_fetch_assoc($result);
		$last24hoursGross  = $last24hours['gross'];
		$last24hoursTax = $last24hours['tax'];
		$last24hoursNet = $last24hours['net'];
		if($last24hoursGross < 1) {
			$line3 = "Last 24 hours: No customers in this period";
		} else {
			$line3 = "Last 24 hours: $" . $last24hoursGross. " gross, $" . $last24hoursTax . " tax, $" . $last24hoursNet . " net.";
		}
		
	} else {
		$line3 = "Last 24 hours: No customers in this period";
	}


$sql = "SELECT SUM(gross) AS gross, SUM(tax) AS tax, SUM(net) AS net FROM vendorprim_trans WHERE (timestamp > DATE_SUB(now(), INTERVAL 30 DAY)) AND uniqueCode = \"" . $uniqueCode . "\" AND objectOwnerUUID = \"" . $objectOwnerUUID . "\"";


	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		$last30days = mysqli_fetch_assoc($result);
		$last30daysGross  = $last30days['gross'];
		$last30daysTax = $last30days['tax'];
		$last30daysNet = $last30days['net'];
		if($last30daysGross < 1) {
			$line4 = "Last 30 days: No customers in this period";
		} else {
			$line4 = "Last 30 days: $" . $last30daysGross. " gross, $" . $last30daysTax . " tax, $" . $last30daysNet . " net.";
		}
	} else {
		$line4 = "Last 30 days: No customers in this period";
	}



$sql = "SELECT SUM(gross) AS gross, SUM(tax) AS tax, SUM(net) AS net FROM vendorprim_trans WHERE uniqueCode = \"" . $uniqueCode . "\" AND objectOwnerUUID = \"" . $objectOwnerUUID . "\"";


	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		$allTime = mysqli_fetch_assoc($result);
		$allTimeGross  = $allTime['gross'];
		$allTimeTax = $allTime['tax'];
		$allTimeNet = $allTime['net'];
		if($allTimeGross < 1) {
			$line5 = "All time: No customers in this period.";
		} else {
			$line5 = "All time: $" . $allTimeGross. " gross, $" . $allTimeTax . " tax, $" . $allTimeNet . " net.";
		}
	} else {
		$line5 = "All time: No customers in this period.";
	}


	die("@@@VP@@@|ReportLine1|" . $line1 . "|ReportLine2|" . $line2 . "|ReportLine3|" . $line3 . "|ReportLine4|" . $line4 . "|ReportLine5|" . $line5);

?>
